import os
import sys
import csv
import warnings
from typing import Union
from typing import List
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
import requests
from pmdarima import auto_arima

pd.plotting.register_matplotlib_converters()
matplotlib.style.use("seaborn")


class EnergyData:
    """
    Contains a dataset containing a collection of key metrics on world-wide energy data:
    energy consumption, energy mix, electricity mix and other metrics.
    Further it has several methods allowing to perform analyses of the dataset.

    Attributes
    ----------
    dataset: Pandas DataFrame
        the dataset in Pandas DataFrame form

    Methods
    --------
    __init__()
        Calls df_creation and assigns the returned dataset to self.dataset.
    df_creation()
        Downloads (if not downlaoded yet) and creates the Pandas DataFrame.
    method2()
        Returns a list including all available countries in the dataset.
    method3()
        Plots an area chart comparing the consumption of energy types per year since recording.
        The plot can be shown as absolute or relative.
    method4()
        Plots the total consumption of the specified countries.
    method5()
        Plots the GDP of the specified countries over the years.
    gapminder()
        Plots a scatter plot where x is gdp, y is the total energy consumption, and the area of
        each dot is population.
    arimaprediction()
        Plots an line chart chart displaying the historical as well as the predicted energy
        consumption and emissions of a selected country.
    scatter()
        Plots a scatter plot between emissions and consumption for all countries per year.
    """

    def __init__(self):
        self.dataset = self.method_1()

    def method_1(self):
        """
        Checks whether the data has been downloaded already. If not, it creates the
        folder 'downloads' and executes the download.
        Then saves the downloaded CSV file into a pandas DataFrame, selecting
        only data from 1970 on (incusively), transforms the year to a datetime object
        and calcualtes emissions per energy type and in total.

        Parameters
        -----------
            Nothing, apart from self.

        Returns
        --------
            Pandas DataFrame containing all data from the year 1970 on (inclusively).
        """

        url = "https://nyc3.digitaloceanspaces.com/owid-public/data/energy/owid-energy-data.csv"
        response = requests.get(url)
        # path definition
        ourpath = os.path.join(os.getcwd(), "downloads")
        filename = "downloaded_data.csv"
        fullpath = os.path.join(ourpath, filename)

        # try except finally statement: if the file exists and can be opened, it directly goes
        #to next step and closes it again
        # if it does not exist, error is raised and we go into the except statement from
        # where the downloads folder is created and the file is downloaded
        try:
            with open(fullpath, encoding = 'utf-8') as helpfile:
                print('The datafile has been downloaded already!')

        except EnvironmentError:
            if not os.path.exists(ourpath):
                os.mkdir(ourpath)
            sys.path.append("./downloads/")
            with open(fullpath, "w", encoding = 'utf-8', ) as file:
                writer = csv.writer(file)
                for line in response.iter_lines():
                    writer.writerow(line.decode("utf-8").split(","))
            file.close()

        else:
            helpfile.close()

        # transformation of the data into df, select only rows with year > 1969
        sys.path.append("./downloads/")
        dataframe = pd.read_csv(fullpath)
        dataframe = dataframe[dataframe["year"] > 1969].reset_index(drop=True)
        dataframe["year"] = pd.to_datetime(dataframe["year"], format="%Y")
        dataframe.set_index("year", drop=True, inplace=True)
        # Readding year as column as code from previous iteration need the column to work
        dataframe["year"] = dataframe.index.year

        # Enriching the dataframe with emission per energy type and the total
        dataframe["biofuel_emissions"] = dataframe["biofuel_consumption"] * 1450 * 1000
        dataframe["coal_emissions"] = dataframe["coal_consumption"] * 1000 * 1000
        dataframe["gas_emissions"] = dataframe["gas_consumption"] * 455 * 1000
        dataframe["hydro_emissions"] = dataframe["hydro_consumption"] * 90 * 1000
        dataframe["nuclear_emissions"] = dataframe["nuclear_consumption"] * 5.5 * 1000
        dataframe["oil_emissions"] = dataframe["oil_consumption"] * 1200 * 1000
        dataframe["solar_emissions"] = dataframe["solar_consumption"] * 53 * 1000
        dataframe["wind_emissions"] = dataframe["wind_consumption"] * 14 * 1000
        dataframe["total_emissions"] = dataframe.filter(regex="_emissions").sum(axis=1)

        return dataframe

    def method_2(self):
        """
        Returns a list of available countries in the data set.

        Parameters
        ---------------
            Nothing, apart from self.

        Returns
        ---------------
            List of unique countries in column "country".
        """

        return list(np.unique(self.dataset["country"]))

    def method_3(self, country: str, normalized: bool = False):
        """
        Plots an area chart comparing the consumption of energy types since recording started.
        The plot can be created in absolute or relative manner.

        Parameters
        ---------------
        country: str
            The country upon which the request is made.
        normalized: bool
            If true, normalizes the consumption in relative terms: each year,
            consumption is 100%. Default is False.

        Returns
        ---------------
            Nothing, the function only plots the graph.
        """

        if country not in self.method_2():
            raise KeyError(f"Chosen country {country} does not exist.")

        # Get country-specific data
        df_country = self.dataset.loc[(self.dataset["country"] == country)]

        # Remove unneccesary columns (some consumption columns are the
        # sum of others and are therefore dropped)
        df_country_consumption = pd.concat(
            [
                df_country[["iso_code", "country", "year"]],
                df_country.filter(regex="_consumption"),
            ],
            axis=1,
        )
        df_country_consumption.drop(
            [
                "fossil_fuel_consumption",
                "low_carbon_consumption",
                "primary_energy_consumption",
                "renewables_consumption",
            ],
            axis=1,
            inplace=True,
        )
        df_country_consumption.dropna(axis=0, how="all", thresh=4, inplace=True)
        title = country + "'s Energy Consumption by Type"

        # Include normalized argument
        if normalized is True:
            df_country_consumption = pd.concat(
                [
                    df_country_consumption[["iso_code", "country", "year"]],
                    df_country_consumption.iloc[:, 3:].apply(
                        lambda x: x / x.sum(), axis=1
                    ),
                ],
                axis=1,
            )
            title = country + "'s Energy Consumption by Type (normalized)"

        # Plot area chart
        df_country_consumption.plot.area(
            x="year",
            stacked=True,
            figsize=(15, 10),
            title=title,
            grid=True,
            xlabel="Year",
            ylabel="Consumption",
        )

    def method_4(self, countries: Union[List[str], str]):
        """This function receives a string with a country or a list of country strings.
        It plots the total of the "_consumption" column of the specified countries.

        Parameters
        ---------------
        countries: str/List[str]
            The country or countries to plot.

        Returns
        ---------------
            Nothing, the function only plots the graph.
        """

        # Raises an error if countries argument is not str or list
        if type(countries) not in [str, list]:
            raise TypeError("Variable 'countries' is not a String or List.")

        # Converting countries to a list of strings, if it is a str
        if isinstance(countries, str):
            countries = countries.split("delimiter")

        # Raising an error if a country input is not in the dataset
        for country in countries:
            if country not in self.method_2():
                raise KeyError(f"Chosen country {country} does not exist.")

        # Using 'primary energy consumption' as this is the total consumption and aggregating it by
        df_aggregate = (
            self.dataset[["primary_energy_consumption", "country"]]
            .groupby(by="country")
            .sum()
            .reset_index()
        )

        # Select the countries to be plotted
        df_aggregate_selected = df_aggregate[df_aggregate["country"].isin(countries)]

        df_aggregate_selected.plot(
            x="country",
            y="primary_energy_consumption",
            kind="bar",
            figsize=(15, 5),
            title="Countries by total consumption",
            grid=True,
            xlabel="Countries",
            ylabel="Consumption in TWh",
            legend=False,
        )

    def method_5(self, countries: Union[List[str], str]):
        """This function receives a string with a country or a list of country strings.
        It plots the "gdp" column of the specified countries over the years.

        Parameters
        ---------------
        countries: str/List[str]
            The country or countries to plot.

        Returns
        ---------------
            Nothing, the function only plots the graph.
        """

        # Raises an error if countries argument is not str or list
        if type(countries) not in [str, list]:
            raise TypeError("Variable 'countries' is not a String or List.")

        # Converting countries to a list of strings, if it is a str
        if isinstance(countries, str):
            countries = countries.split("delimiter")

        # Raising an error if a country input is not in the dataset
        for country in countries:
            if country not in self.method_2():
                raise KeyError(f"Chosen country {country} does not exist.")

        # Select the countries to be plotted
        df_gdp_selected = self.dataset[self.dataset["country"].isin(countries)]

        df_gdp_selected = df_gdp_selected[["country", "year", "gdp", "total_emissions"]]
        df_gdp_selected = df_gdp_selected.pivot(
            index="year", columns="country", values=["gdp", "total_emissions"]
        )

        # Plot the countries with a secondary y axis
        ax1 = plt.subplots(figsize=(15, 8))[1]

        ax1.set_title("GDP and emissions for " + ', '.join(countries))
        ax1.set_xlabel("Years")
        ax1.set_ylabel("GDP in USD")
        ax1.plot(df_gdp_selected.index[:-2], df_gdp_selected["gdp"][:-2])
        ax1.legend(df_gdp_selected.gdp.columns + " gdp", loc="upper left")

        ax2 = ax1.twinx()

        ax2.set_ylabel("Total Emissions in tCO2")
        ax2.plot(
            df_gdp_selected.index[:-2],
            df_gdp_selected["total_emissions"][:-2],
            linestyle="--",
        )
        ax2.legend(
            df_gdp_selected.total_emissions.columns + " emissions", loc="upper center"
        )

        plt.show()

    def gapminder(self, year: int):
        """This function receives an int representing a year.
        This method should plot a scatter plot where x is gdp, y is the total energy
        consumption, and the area of each dot is population.

        Parameters
        ---------------
        year: int
            The year to plot as gapminder.

        Returns
        ---------------
            Nothing, the function only plots the graph.
        """

        warnings.filterwarnings("ignore")

        if isinstance(year, int):
            copy = self.dataset[self.dataset["year"] == year]

            # Drop cummulated rows
            cummulated = [
                "Africa",
                "Asia Pacific",
                "CIS",
                "Central America",
                "Eastern Africa",
                "Europe",
                "Europe (other)",
                "Middle Africa",
                "Middle East",
                "North America",
                "OPEC",
                "Other Asia & Pacific",
                "Other CIS",
                "Other Caribbean",
                "Other Northern Africa",
                "Other South America",
                "Other Southern Africa",
                "South & Central America",
                "Western Africa",
                "World",
            ]
            copy = copy[copy.country.isin(cummulated) == False]

            gdp = copy.gdp

            relevant_columns = list(filter(lambda x: "_consumption" in x, copy.columns))
            copy["total_consumption"] = copy[relevant_columns].sum(axis=1, skipna=True)

            consumption = copy.total_consumption

            area = copy.population

            # Increase the graph size
            plt.figure(dpi=150)
            # Store population as a numpy array: np_pop
            np_pop = np.array(area)
            np_pop2 = np_pop * 2
            # Let's delete the annoying legend
            sns.scatterplot(
                x = gdp,
                y = consumption,
                hue=copy["country"],
                legend=False,
                size=np_pop2,
                sizes=(20, 500),
                alpha=0.8,
            )
            plt.grid(True)
            plt.xscale("log")
            plt.xlabel("GDP", fontsize=14)
            plt.ylabel("total_consumption", fontsize=14)
            plt.title("Year " + str(year), fontsize=20)
            plt.xticks(
                [500000000, 1000000000, 10000000000, 100000000000, 1000000000000],
                ["500m", "1b", "10b", "100b", "1tr"],
            )
            plt.text(
                copy.gdp[copy.country == "China"],
                copy.total_consumption[copy.country == "China"],
                "China",
            )
            plt.text(
                copy.gdp[copy.country == "United States"],
                copy.total_consumption[copy.country == "United States"],
                "United States",
            )
            plt.text(
                copy.gdp[copy.country == "Germany"],
                copy.total_consumption[copy.country == "Germany"],
                "Germany",
            )
            plt.show()
        else:
            raise ValueError("ValueError exception thrown")

    def arimaprediction(self, country: str, prediction_periods: int = 5):
        """
        Plots an line chart chart displaying both energy consumption and emissions
        since recording started.
        Then uses the Auto AMIRA model to predict mentioned values for a set number
        of years, which is added to the plot.

        Parameters
        ---------------
        country: str
            The country upon which the request is made.
        prediction_periods: int
            Number of periods for which the energy consumption and emissions are predicted.
            The defaut value is 5.

        Returns
        ---------------
            Nothing, the function only plots the graph.
        """

        warnings.filterwarnings("ignore")

        if not isinstance(prediction_periods, int):
            raise TypeError("Prediction period must be an integer.")

        # Raise error for invalid prediction period
        if prediction_periods < 1:
            raise ValueError("Prediction period must be at least 1.")


        # Raise error for invalid country
        if country not in self.method_2():
            raise KeyError("Chosen country %s does not exist." % country)

        # Get country-specific data
        df_country = self.dataset[(self.dataset["country"] == country)][
            ["country", "primary_energy_consumption", "total_emissions"]
        ]
        df_country.dropna(axis=0, inplace=True)

        # Use Auto-ARIMA
        prediction = pd.DataFrame()
        prediction["Time"] = pd.date_range(
            start="2020-01-01", periods=prediction_periods, freq="YS"
        )

        for column in ["primary_energy_consumption", "total_emissions"]:
            arima_model = auto_arima(
                df_country[column],
                start_p=0,
                start_q=0,
                max_p=5,
                max_q=5,
                seasonal=False,
                d=None,
                error_action="ignore",
                suppress_warnings=True,
                stepwise=False,
            )

            prediction[column] = pd.DataFrame(
                arima_model.predict(n_periods=prediction_periods)
            )

        prediction.set_index("Time", inplace=True)
        prediction.columns = ["Predicted consumption", "Predicted emissions"]

        # Plot outcome
        fig, ax1 = plt.subplots(figsize=(15, 8))

        ax1.set_xlabel("Year")
        ax1.set_ylabel("Consumption")
        ax1.plot(
            df_country["primary_energy_consumption"],
            color="blue",
            label="Energy Consumption",
        )
        ax1.plot(
            prediction["Predicted consumption"],
            color="blue",
            linestyle="dashed",
            label="Predicted Energy Consumption",
        )

        ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

        ax2.set_ylabel("Emissions")
        ax2.plot(df_country["total_emissions"], color="green", label="Emissions")
        ax2.plot(
            prediction["Predicted emissions"],
            color="green",
            linestyle="dashed",
            label="Predicted Emissions",
        )

        fig.legend(loc=2, borderaxespad=9)
        plt.title(
            "ARIMA Prediction for Energy Consumption and Emissions of " + country,
            fontsize=20,
        )
        plt.show()

    def scatter(self, year: int):
        """
        Plots a scatter plot between emissions and consumption for all countries showing
        the size of the markers as the population.

        Parameters
        ---------------
        year: int
            Year for which the the scatter plot should be created.

        Returns
        ---------------
            Nothing, the function only plots the graph.
        """
        # Raise error for invalid prediction period
        if not isinstance(year, int):
            raise TypeError("Year must be an integer.")

        # Get year-specific data
        df_year = self.dataset[(self.dataset["year"] == year)][
            ["country", "population", "primary_energy_consumption", "total_emissions"]
        ]
        df_year.dropna(axis=0, inplace=True)

        # Drop cummulated rows
        cummulated = [
            "Africa",
            "Asia Pacific",
            "CIS",
            "Central America",
            "Eastern Africa",
            "Europe",
            "Europe (other)",
            "Middle Africa",
            "Middle East",
            "North America",
            "OPEC",
            "Other Asia & Pacific",
            "Other CIS",
            "Other Caribbean",
            "Other Northern Africa",
            "Other South America",
            "Other Southern Africa",
            "South & Central America",
            "Western Africa",
            "World",
        ]
        df_year = df_year[df_year.country.isin(cummulated) == False]
        df_year['Population in Million'] = df_year.population / 1000000

        # Plot
        plt.figure(figsize=(15, 10))
        sns.scatterplot(
            data=df_year,
            x="primary_energy_consumption",
            y="total_emissions",
            size="Population in Million",
            alpha=0.7,
            sizes=(20,500)
        )
        plt.xlabel("Total Energy Consumption", fontsize=14)
        plt.ylabel("Total Emissions", fontsize=14)
        plt.title(
            "Total Emissions and Energy Consumption of Year " + str(year), fontsize=20
        )
        plt.xscale("log")
        plt.yscale("log")
        plt.show()
