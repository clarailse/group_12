# Group_12

Group Project 'Ergo': EneryData across the World.
Presented by Business Analytics students and specialists Jens-Gabriel Kohl (48414), Patrick Gundlach (49594), Sebastian Simmel (48671) and Clara Boerner (44427). Please see Support / Contact below and reach out to us if you have any questions or enquiries!


## Ergo
Ergo is a data analysis project, which allows the user to discover and analyse worldwide Energy Data from a database maintained by [Our World in Data](https://ourworldindata.org/energy), which can be inspected on Gitlab under the following link: https://github.com/owid/energy-data

## Description
Within the Ergo project, the user can familiarize with the Energy Data within the Showcase Notebook, which is located in the master level together with the Readme and Licenses files. The notebook displays for example analyses of the consumption of energy types per country or the evolution of energy and GDP consumption as well as a recommendation for decarbonisation.

## Installation
In order to view the output of the project analyses, please run the **Showcase-Notebook** within the Group_12 folder in Jupyter-lab. 

## Support / Contact
Please contact our team under 44427@novasbe.pt if you need any help!

## Roadmap
This project aims to making a better world by making people curious about energy and its impact by making them aware of global energy consumption!

## Contributing
Please fund us at [Paypal](https://paypal.me/PatrickGundlach)!

## Authors and acknowledgment
Authors: Patrick Gundlach, Sebastian Simmel, Jens-Gabriel Kohl, Clara Boerner

## License
This is an open source project, which is licensed by the GNU AFFERO GENERAL PUBLIC LICENSE.
