import os
import csv
import requests
import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt

pd.plotting.register_matplotlib_converters()
matplotlib.style.use("seaborn")


class EnergyData:
    """
    Contains a dataset containing a collection of key metrics on world-wide energy data:
    energy consumption, energy mix, electricity mix and other metrics.
    Further it has several methods allowing to perform analyses of the dataset.

    Attributes
    ----------
    dataset: Pandas DataFrame
        the dataset in Pandas DataFrame form

    Methods
    --------
    df_creation()
        Downloads (if not downlaoded yet) and creates the Pandas DataFrame.
    method2()
        Returns a list including all available countries in the dataset.
    method3()
        Plots an area chart comparing the consumption of energy types per year since recording.
        The plot can be shown as absolute or relative.
    method4()
        Plots the total consumption of the specified countries.
    method5()
        Plots the GDP of the specified countries over the years.
    gapminder()
        Plots a scatter plot where x is gdp, y is the total energy consumption, and the area of
        each dot is population.
        """

    def __init__(self):
        self.dataset = self.method_1()

    def method_1(self): # OPEN Q TO PROF: unsure about how to define pandas df as mypy return type
        """
        Checks whether the data has been downloaded already. If not, it creates the
        folder 'downloads' and executes the download.
        Then saves the downloaded CSV file into a pandas DataFrame, selecting
        only data from 1970 on (incusively).

        Parameters
        -----------
        self:class
            The EnergyData class itself

        Returns
        --------
        Pandas DataFrame containing all data from the year 1970 on (inclusively).
        """
        url = "https://nyc3.digitaloceanspaces.com/owid-public/data/energy/owid-energy-data.csv"
        response = requests.get(url)
        path_parent = os.path.dirname(
            os.getcwd()
        )  # this is meantime solution when in prototypes folder,
        #should not be needed when in master folder anyways-delete?
        os.chdir(
            path_parent
        )  # this is meantime solution when in prototypes folder,
        #should not be needed when in master folder anyways-delete?
        path = os.path.join(os.getcwd(), "downloads")
        filename = "downloaded_data.csv"

        # try except finally statement: if the file folder exists, it directly goes to next step
        # if it does not exist, error is raised and we go into the except statement from
        # where the downloads folder is created and the file is downlaoded
        try:
            os.chdir(path)

        except:
            os.mkdir(path)
            os.chdir(path)
            with open(os.path.join(path, "downloaded_data.csv"), "w") as file:
                writer = csv.writer(file)
                for line in response.iter_lines():
                    writer.writerow(line.decode("utf-8").split(","))
            file.close()

        # transformation of the data into df, select only rows with year > 1970
        filename = "downloaded_data.csv"
        dataframe = pd.read_csv(filename)
        dataframe = dataframe[dataframe["year"] > 1969].reset_index(drop=True)
        return dataframe

    def method_2(self):
        """
        Returns a list of available countries in the data set.

        Parameters
        ---------------
            Nothing.

        Returns
        ---------------
            List of unique countries in column "country"

        """
        return list(np.unique(self.dataset["country"]))

    def method_3(self, country: str, normalized: bool = False):
        """
        Plots an area chart comparing the consumption of energy types since recording started.
        The plot can be created in absolute or relative manner.

        Parameters
        ---------------
        country: str
            The country upon which the request is made
        normalized: bool
            If true, normalizes the consumption in relative terms: each year, consumption is 100%.

        Returns
        ---------------
        Nothing, the function only plots the graph.
        """

        if country not in method_2(self):
            raise KeyError("Chosen country does not exist.")

        # Get country-specific data
        df_country = self.dataset.loc[(self.dataset["country"] == country)]

        # Remove unneccesary columns (some consumption columns are the
        #sum of others and are therefore dropped)
        df_country_consumption = pd.concat(
            [
                df_country[["iso_code", "country", "year"]],
                df_country.filter(regex="_consumption"),
            ],
            axis=1,
        )
        df_country_consumption.drop(
            [
                "fossil_fuel_consumption",
                "low_carbon_consumption",
                "primary_energy_consumption",
                "renewables_consumption",
            ],
            axis=1,
            inplace=True,
        )
        df_country_consumption.dropna(axis=0, how="all", thresh=4, inplace=True)

        # Include normalized argument
        if normalized is True:
            df_country_consumption = pd.concat(
                [
                    df_country_consumption[["iso_code", "country", "year"]],
                    df_country_consumption.iloc[:, 3:].apply(
                        lambda x: x / x.sum(), axis=1
                    ),
                ],
                axis=1,
            )

        # Plot area chart
        df_country_consumption.plot.area(
            x="year",
            stacked=True,
            figsize=(15, 10),
            title=(country + "'s Energy Consumption by Type"),
            grid=True,
            xlabel="Year",
            ylabel="Consumption",
        )

    def method_4(self, countries: [str, list]):
        """This function receives a string with a country or a list of country strings.
        It plots the total of the "_consumption" column of the specified countries.
        Args:
            s (string): country string
        Returns:
            nothing, the function only plots the graph.
        """

        # Raises an error if countries argument is not str or list
        if type(countries) not in [str, list]:
            raise TypeError("Variable 'countries' is not a String or List.")

        # Converting countries to a list of strings, if it is a str
        if isinstance(countries, str):
            countries = countries.split("delimiter")

        # Create the total consumption column based on 'primary energy consumption'
        df_aggregate = self.dataset.groupby(by="country").sum().reset_index()

        # all_columns = list(df_aggregate.columns)
        # relevant_columns = list(filter(lambda x: '_consumption' in x, all_columns))

        # These lines were previously used to sum the consumption columns.
        # However this results in a false sum, beacuse the data set already holds sums
        # of the "_consumption" columns.
        # For example the column 'primary_energy_consumption' already is a sum of all
        # the primary energy sources.
        # Therefore we are going to use it as "total_consumption"

        relevant_columns = ["primary_energy_consumption"]
        df_aggregate["total_consumption"] = df_aggregate[relevant_columns].sum(
            axis=1, skipna=True
        )
        # Select the countries to be plotted
        df_aggregate_selected = df_aggregate[df_aggregate["country"].isin(countries)]

        df_aggregate_selected.plot(
            x="country",
            y="total_consumption",
            kind="bar",
            figsize=(15, 5),
            title="Countries by total consumption",
            grid=True,
            xlabel="Countries",
            ylabel="Consumption in xx",
            legend=False,
        )  # Add colering and seaborn style.

    def method_5(self, countries: str):
        """This function receives a string with a country or a list of country strings.
        It plots the "gdp" column of the specified countries over the years.
        Args:
            s (string): country string
        Returns:
            nothing, the function only plots the graph.
        """

        # Converting countries to a list of strings, if it is a str
        if isinstance(countries, str):
            countries = countries.split("delimiter")

        # Select the countries to be plotted
        df_gdp_selected = self.dataset[self.dataset["country"].isin(countries)]

        df_gdp_selected = df_gdp_selected[["country", "year", "gdp"]]
        df_gdp_selected = df_gdp_selected.pivot(
            index="year", columns="country", values="gdp"
        )

        # Plot the countries
        df_gdp_selected.plot(
            figsize=(15, 5),
            title="Countries by GDP over years",
            grid=True,
            xlabel="Years",
            ylabel="GDP",
            legend=True,
        )

    def __gapminder__(self, year: int):
        """This function receives an int representing a year.
        This method should plot a scatter plot where x is gdp, y is the total energy
        consumption, and the area of each dot is population.
        Args:
            year (int): year int
        Returns:
            nothing, the function only plots the scatterplot.
        """

        if isinstance(year, int):
            copy = self.dataset[self.dataset["year"] == year]
            x = copy.gdp

            relevant_columns = list(filter(lambda x: "_consumption" in x, copy.columns))
            copy["total_consumption"] = copy[relevant_columns].sum(axis=1, skipna=True)

            y = copy.total_consumption

            area = copy.population

            # Increase the graph size
            plt.figure(dpi=150)
            # Store population as a numpy array: np_pop
            np_pop = np.array(area)
            np_pop2 = np_pop * 2
            # Let's delete the annoying legend
            sns.scatterplot(
                x,
                y,
                hue=copy["country"],
                legend=False,
                size=np_pop2,
                sizes=(20, 500),
                alpha=0.8,
            )
            plt.grid(True)
            plt.xscale("log")
            plt.xlabel("GDP", fontsize=14)
            plt.ylabel("total_consumption", fontsize=14)
            plt.title("Year " + str(year), fontsize=20)
            plt.xticks(
                [500000000, 1000000000, 10000000000, 100000000000, 1000000000000],
                ["500m", "1b", "10b", "100b", "1tr"],
            )
            plt.text(
                copy.gdp[copy.country == "China"],
                copy.total_consumption[copy.country == "China"],
                "China",
            )
            plt.text(
                copy.gdp[copy.country == "United States"],
                copy.total_consumption[copy.country == "United States"],
                "United States",
            )
            plt.text(
                copy.gdp[copy.country == "Germany"],
                copy.total_consumption[copy.country == "Germany"],
                "Germany",
            )
            plt.show()
        else:
            raise ValueError("ValueError exception thrown")
